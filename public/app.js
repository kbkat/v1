import { ResultList } from './components/resultcodes.js'
import { ExcelPage } from './components/excel.js'
import { QuaggaConfig } from './components/config.js'
import { MixinDebug } from './components/mixin.js'
import { PageLoader } from './components/pageloader.js'
import { ZMDIcon } from   './components/icon.js'
import { UpdateWorker } from './components/worker.js'

const App = {
  components: {
    quagga: QuaggaConfig,
    excel: ExcelPage,
    results: ResultList,
    pageloader: PageLoader,
    zmicon: ZMDIcon,
    worker: UpdateWorker,
  },

  data : function () {
    return {
      excelConfig: null,
      quaggaConfig: null,
      buttons: [
	{ caption: "config" ,  show:false },
	{ caption: "excel",  show: false},
	{ caption: "scanner",  show: false},
      ],
      update: {update: false},
      ver: '---'
    }
  },
  
  template: `
    <div class="contents">
    <div class="navbar">
    <div class="navbar-menu columns is-mobile is-active">
    <div class="navbar-start">
    <div class="column" v-for="btn in buttons" >
         <input class="button is-link" type="button" @click="btn.show=true" :value="btn.caption">
    </div>
    </div>
    <div class="navbar-end">
    <div class="column"  style="padding-right:2em;">
      <a @click="appUpdate">Ver.{{versionStr}}</a>
   </div>
    </div>
    </div>
    </div>
  <section class="columns">
    <div class="column">
      <results debug="all" :excelConfig="excelConfig" :quaggaConfig="quaggaConfig" :showScanner="buttons[2].show">
    <div style="position:absolute;height:100%;left:90%;z-index:99999;padding:auto;margin-top:60vh">
<zmicon @click="buttons[2].show=false" i="close-circle-o" :x="5" c="#babbcd"></zmicon>
    </div>
      </results>
    </div>
  </section>
     <pageloader :class="{'is-active':buttons[0].show}" style="background:#eedd66;padding-left:20%;padding-right:20%">
	<quagga @update="setQuagga"></quagga>
     </pageloader>
     <pageloader :class="{'is-active':buttons[1].show}" style="padding-left:30%;background:#aacc88">
	<excel @update="setExcel" debug="excel"></excel>
     </pageloader>
    </div>
    <worker style="display:none" @version="setVersion" @update="updateNotify" :doupdate="update.doUpdate"></worker>
  `,
  computed: {
    versionStr: function() {
      console.log(this.ver);
      console.log(this.update);
      let str = this.ver + (this.update.update ? "=>"+this.update.app: "")
      console.log(str);
      return str;
    }
  },
  
  methods : {
    setVersion(ver) {
      console.log("set version " + ver)
      this.ver = ver
    },
    updateNotify(data) {
      this.update.app = data;
      this.update.update=true
    },
    appUpdate() {
      this.update.doUpdate = this.update.update ? 'update':'check'
    },

    setExcel(config) {
      this.excelConfig = config
      this.buttons[1].show=false
    },
    setQuagga(config){
      this.quaggaConfig=config
      this.buttons[0].show=false
    },
  }
};

export default (function() {
  return Vue.createApp({
    components: {
      app: App
    },
  })
})()
