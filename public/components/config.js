import { load, persist } from '../js/Storage.js'
import { MixinDebug } from './mixin.js'
import { ConfigList, ConfigInput } from './configinput.js'

const quaggaConfig = load('quaggaConfig') || {}

const cleanConfig = config => {
  if (typeof config.inputStream.constraints.deviceId === 'number') {
    config.inputStream.constraints.deviceId = null;
  }
  return config;
};

export const defaultConfig = {
  frequency: 5,
  numOfWorkers: 2,
  locate: true,
  inputStream: { 
    name: "Live",
    type: "LiveStream",
    constraints: {
      width: 800,
      height: 600,
      deviceId: 0,
      facingMode: "environment",
    },
    area: {
      top: "0%",
      right: "0%",
      left: "0%",
      bottom: "0%",
    },
  },
  decoder: {
    readers: [
      'ean_reader',
      'code_39_reader',
      'code_128_reader',
    ],
    /*
    debug: {
      drawBoundingBox: false,
      showFrequency: false,
      drawScanline: false,
      showPatter: false,
    },
    */
    multiple: false
  },
  locator: {
    halfSample: true,
    patchSize: "medium",
    /*
    debug: {
      showCanvas: false,
      showPatches: false,
      showFoundPatches: false,
      showSkeleton: false,
      showLabels: false,
      showPatchLabels: false,
      showRemainingPatchLabels: false,
      boxFromPatches: {
	showTransformed: false,
	showTransformedBox: false,
	showBB: false
      }
    }
    */
  },
};

export const QuaggaConfig = {

  components : { clist: ConfigList, cinput: ConfigInput },
  mixins: [MixinDebug],
  props: {
    debug: MixinDebug.$types,
  },
  data: function () {
    //    this.setDebug(this.debug, 'config')
    this.setDebug('config', 'config')    
    this.formatList = [ "ean", "ean_8","code_39",
			"codabar", "code_128","i2of5"]
    return  {
      inputs : [
	{ type: "select",
  	  title: "サイズ",
	  name: "sizeText",
	  list: [ "640x480", "800x600", "1280x960" ],
	},
	{ type: "select",
	  title: "Frequency",
	  name: "frequency",
	  list: [5,6,7,8,9,10,11,12,13,14,15],
	},
	{ type: "select",
	  title: "Workers",
	  name: "numOfWorkers",
	  list: [1,2,3,4,5],
	},
	{ type: "checkbox",
	  title: "Locate",
	  name: "locate",
	},
	{ type: "select",
	  title: "エリア",
	  name: "area",
	  list: ['0%','5%','10%','15%'],
	},
	{ type: "select",
	  title: "パッチサイズ",
	  name: "locator.patchSize",
	  list: ["small","medium","large"],
	},
	{ type: "checkbox",
	  title: "HalfSample",
	  name: "locator.halfSample",
	}
      ],
      config : quaggaConfig.config || this.configDefault(),
      cl: null,
      buttons: [
	{ caption: "保存",
	  callback: this.save
	},
	{ caption: "キャンセル",
	  callback: this.cancel
	},
	{ caption: "デフォルト",
	  callback: this.setDefault
	},
      ],
    }
  },
  
  template: `
    <div style="margin-left:auto;margin-right:auto;" >
    <clist :debug="debug" :config="config" :inputs="inputs"></clist>
            <p>フォーマット</p>
    <cinput v-for="fm in Object.keys(config.formats)"
      :debug="debug" :conf="config" type="checkbox" :title="fm" :name="'formats.'+fm"></cinput>
    <div class="columns is-mobile" style="width:50vw;background:#eeeeee">
      <a class="column is-link" v-for="btn in buttons" @click="btn.callback" style="width:20%">{{ btn.caption }}</a>
    </div>
      <div style="visibility:hidden">
	スキャナー設定
	</div>
    </div>
  `,

  emits: ['update'],
  
  computed: {
  },

  mounted() {
    this.$emit('update',this.config);
  },

  
  beforeUpdate() {
    this.logger("update config",this.config);
  },

  beforeMount() {
    this.logger("mount", this.config)
  },
  methods: {
    click() {

    },
    setDefault () {
      this.config = Object.assign({}, this.configDefault())
      this.inputs = [...this.inputs]
      this.logger("setDefault",this.config);
    },

    cancel() {
      this.$emit('update',this.config);
    },

    save() {
      let c = this.config.inputStream.constraints
      let s = this.config.sizeText
      let i = s.indexOf("x")
      c.width=parseInt(s.substring(0,i))
      c.height=parseInt(s.substring(i+1))
      let a = this.config.area
      this.config.inputStream.area={top: a, right:a, bottom:a, left:a}
      let selected = this.config.formats
      this.config.decoder.readers= this.formatList.filter(fm=>selected[fm]).map(fm=>fm+"_reader");
      quaggaConfig.config = this.config
      persist('quaggaConfig',quaggaConfig);
      this.logger("save",this.config);
      this.$emit('update',this.config);
    },
    
    configDefault() {
      let conf = defaultConfig;
      conf.sizeText="800x600"
      conf.area='0%'
      conf.formats={}
      this.formatList.forEach(fm=>{
	conf.formats[fm] = (conf.decoder.readers.indexOf(fm+"_reader")>=0)
      })
      return conf;
    },

  },
}

  /*

    readerName: function(name) {
      return name.substring(0,name.length-'_reader'.length).toUpperCase()
    },
    readerCheck: function(key) {
      let readers = quagga_config.decoder.readers;
      let i = readers.indexOf(key);
      if( this.formats[key] && i < 0 ) {
	readers.splice(0,0,key)
      } else if( i>=0 ) {
	readers.splice(i,1)
      }
      console.log(quagga_config.decoder.readers);
    },
    selectedSize: function (size) {
      let constraints = quagga_config.inputStream.constraints
      return "" + constraints.width +"x"+ constraints.height == size
    },
    sizeChange: function(ev) {
      var s = ev.target.value.split("x")
      quagga_config.inputStream.constraints.width=parseInt(s[0]);
      quagga_config.inputStream.constraints.heihgt=parseInt(s[1]);
      console.log(s)
    },
  },
    */


