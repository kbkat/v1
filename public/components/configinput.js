import { MixinDebug } from './mixin.js'

export const ConfigInput = {
  props: {
    debug: MixinDebug.$types,
    name: String,
    type: String,
    conf: Object,
    options: Array,
    title: String,
  },
  mixins: [MixinDebug],
  data() {
    this.setDebug(this.debug,'config');
    return {
      attr: (function(n) { let i = n.indexOf(".")
	return i>0 ? n.substring(i+1): n})(this.name),
    }
  },

  computed: {
    parent: function(c,n){ let i = this.name.indexOf(".")
      return i>0 ? this.conf[this.name.substring(0,i)]:this.conf},
  },

  template: `
      <div class="field" style="display:flex">
      <label class="label is-small" @clikc="click" style="padding-right:1em">
        {{title?title:name}}
      </label>
      <div v-if="type=='select'" class="select is-link is-rounded is-normal">
        <select v-model="parent[attr]">
        <option v-if="parent[attr]=='noselect'" value="noselect">未選択</option>
	<option v-for="opt in options">{{opt}}</option>
        </select>
      </div>
      <input v-else :type="type" v-model="parent[attr]" is-link is-normal @click="click">
      </div>
    `,

    created() {
    this.logger("parent",this.parent);
    this.logger("attr",this.attr)
  },
  methods : {
    click() {
      this.logger("cinput",this);
    },
  },
  

  beforeUpdate() {
    this.logger("cinput update",this.conf);
  },
  created: function() {
    this.logger("created", this);
  }

};

export const ConfigList= {
  components: {
    cinput : ConfigInput
  },
  props: {
    debug: MixinDebug.types,
    inputs: Array,
    config: Object,
  },
  template: `
  <cinput :debug="debug" v-for="i in inputs" :type="i.type" :name="i.name" :title="i.title" :options="i.list" :conf="config">
  </cinput>
  `,
  unmounted() {
    this.logger("unmount",this);
  },
}
