export const PageLoader = {
  props: {
    buttons: Array,
    activate: Boolean,
    debug: Boolean,
  },
  
  data() {
    if( this.debug) console.log("init");
    return {
      btns: this.buttons
    }
  },

  emits: ['hide'],
  
  template: `
    <div class="pageloader" :class="{'is-active': activate}">
    <div class="pagecontent">
         <slot></slot>
    </div>
    </div>
  `,

  beforeUpdate() {
    if( this.debug ) console.log("updating pageloader");
  },

  methods: {
    callback(func) { if( func() ) this.$emit('hide')  },
  },
}
