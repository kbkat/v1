import { FileUpdater } from "./fileupdater.js"
import { MixinDebug } from "./mixin.js"
  
export const ExcelReader = {
  components: {
    fileupdater: FileUpdater
  },
  mixins: [MixinDebug],

  emits: [ 'load' ],
  props: { debug: MixinDebug.types, saved: Object },

  data() {
    this.setDebug(this.debug,'excelreader')
    return {
      filedata: this.saved
    }
  },
  
  template: `
    <fileupdater @selected="readExcel" :filedata="filedata"></fileupdater>
  `,
  
  methods: {
    dateStr(time) {
      let d = new Date(time)
      return d.getFullYear()+"/"
	    +("0"+(d.getMonth()+1)).slice(-2)+"/"
	    +("0"+(d.getDate())).slice(-2)+" "
	    +("0"+(d.getHours()+1)).slice(-2)+":"
	    +("0"+(d.getMinutes())).slice(-2)
    },

    readExcel(file) {
      this.logger("reading excel", file)
      const reader = new FileReader()
      let fileData = {
	filename: file.name,
	lastModifiedDate: this.dateStr(file.lastModified)
      }
      let _self = this
      
      reader.onload = function(e) {
	let data = (function (data) {
	  var val = "", i=0, w=10240;
	  for( ; i < data.byteLength /w; ++i ) {
	    val += String.fromCharCode.apply(null, new Uint8Array(data.slice(i*w, i*w+w)))
	  }
	  val+= String.fromCharCode.apply(null, new Uint8Array(data.slice(i*w)));
	  return val
	})(e.target.result)

	let wb = XLSX.read(btoa(data),{ type: 'base64', cellDates: true, });
	_self.logger("read workbook", wb);

	fileData.sheets = {}
	wb.SheetNames.forEach( function (name) {
	  let roa = XLSX.utils.sheet_to_json(wb.Sheets[name], { raw: true, })
	  let data = roa.length>0 ? roa:null
	  let columns = !!data ? Object.keys(data[0]):null
	  fileData.sheets[name] = { data: data, columns: columns }
	})
	fileData.namedranges = wb.Workbook.Names
	_self.$emit('load',fileData);
      }
      reader.readAsArrayBuffer(file);
    }
  },
}

