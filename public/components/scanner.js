import { MixinDebug } from "./mixin.js"

export const Scanner = {
  mixins: [MixinDebug],
  props: {
    debug: MixinDebug.types, 
    active: Boolean,
    config: Object,
  },
  emits: ['detect', 'result', 'hide'],
  data : function () {
    this.setDebug(this.debug,"scanner");
    return {
      currentCode: {},
      resultCode: {},
      counter: 0,
    }
  },
  template: `
    <div  style="width:100vw;height:auto; background-color:#555555;position:relative;max-height=80vh">
    <div id="interactive" class="viewport" style="background-color:#ffffff;margin:auto">
    </div>

    </div>
  `,
  beforeUpdate() {
    this.logger("scanner updating",this.active);
  },
  mounted:function() {
    Quagga.onDetected(this.onDetected)
    Quagga.onProcessed(this.onProcessed)
  },
  
  updated: function () {
    this.logger("scanner upadate", this.active)
    if( this.active ) {
      let _self=this
      this.config.inputStream.target = document.querySelector('#quagga')
      Quagga.init(this.config, function(e) {
	Quagga.start()
	_self.logger("scanner", "quagga start");
      });
    } else {
      Quagga.stop()
      this.logger("scanner", "quagga stop");
    }
  },
  methods: {
    start() {
      if( this.active )
	Quagga.init(this.config, function(e) {
	  Quagga.start()
	  this.logger("quagga start", this.config);

	});
    },
    stop() {
      Quagga.stop();
    },

    onDetected: function(result) {
      if( !!result ){
	console.log("detected");
	if( this.currentCode != "" && this.currentCode == result.codeResult.code ) {
	  this.counter++;
	  this.$emit('detect',result);
	  if ( this.counter > 1 ) {
	    this.resultCode = result.codeResult;
	    this.logger('result',result.codeResult)
	    this.$emit('result',{
	      code: result.codeResult.code,
	      format: result.codeResult.format,
	      date: new Date(),
	    })
	  }
	} else {
	  this.currentCode = result.codeResult.code;
	  this.counter=0;
	}
      }
    },
    onProcessed: function(result) {
      var drawingCtx = Quagga.canvas.ctx.overlay,
          drawingCanvas = Quagga.canvas.dom.overlay;
      if (result) {
        if (result.boxes) {
          drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
	  /*
          result.boxes.filter(function (box) {
            return box !== result.box;
          }).forEach(function (box) {
            Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
          });
	  */
        }

	if (result.box) {
          Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
        }
      }
    },
  },
};


export const BarcodeScanner = {
  props: {
    active: Boolean
  },
  emits: ['hide'],
  components: {
    scanner: Scanner,
  },
  data: function () {
    return {
      scannerActive: false,
      result: null,
    }
  },
  template: `
    <div>
   <scanner :active="scannerActive" @closeScanner="$emit('hide')" @saveRecord="saveRecord"></scanner>
   <results :addItem="result" @deleteDate="deleteDate"></results>
    </div>
  `,
  methods : {
    deleteDate : function(date) {
      console.log("delete "+ date)
      let i=0
      while (i<resultCodes.length) {
	console.log("i=" + i +": codes=" + resultCodes.length)
	if( dateString(resultCodes[i].date) == date ) {
	  console.log(dateString(resultCodes[i].date))
	  resultCodes.splice(i,1);
	} else {
	  i++;
	}
      }
      persist('resultCodes',resultCodes);
      console.log("delete persist: len="+resultCodes.length)
      this.result = null
    },
    saveRecord: function(result) {
      console.log("save");
      this.result = {
	key: result.record.key,
	code: result.record.code,
	format: result.record.format,
	data: result.record.data,
	sheet: result.sheet.name,
	inputs: result.inputs,
	date: result.record.date,
      }
    },
    close: function() {
      this.scannerActive = false;
    }
  },
  updated: function() {
    if( this.$props.active != this.isActive ) {
      this.scannerActive = this.$props.active
    }
  }
}
