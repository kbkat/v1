export const ZMDIcon = {
  props: {
    i: String,
    x: Number,
    c: String,
  },
  data () {
    let json = {}
    json['zmdi-'+this.i]=true
    json['zmdi-hc-'+(!this.x ? 0: this.x)+'x']=this.x>0
    return {
      classObj:  json
    }
  },
  template: `<span class="icon"><i class="zmdi" :class="classObj" :style="{color: c}"></i></span>`
  };
