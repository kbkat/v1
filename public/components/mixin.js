export const MixinDebug = {
  types: { String, Number, Array, Boolean },
  data (){
    return {
      debug_level: 0
    }
  },
  computed: {
    isDebugging: function(){return this.debug_level>0 }
  },
  methods: {
    logger(dscr, msg) {
      if( this.debug_level > 0) {
	console.log(dscr)
	console.log(msg)
      }
    },
    
    setDebug(array, name) {
      if( !array ) return
      let type = typeof array
      if( type == 'number' || type == 'boolean') {
	this.debug_level=array
	return
      }
      if( type == 'string' ) {
	if( array == 'all' || array == name ) {
	  this.debug_level = 1
	}
	return
      }
	
      if( type == 'object' ) {
	if( array.indexOf('all') >= 0 || array.indexOf(name) >=0 )
	  this.debug_level = 1
      }
    }
  }
}
	   
    
