// -*- coding: utf-8 -*-
import { MixinDebug } from './mixin.js'
import { DateItems } from './resultitems.js'
import { ScannerControl } from './scannercontrol.js'
import { PageLoader } from './pageloader.js'
import{ load, persist } from "../js/Storage.js"

const dateResults = [
  { code: "123",
    data: {"code": 123, name: "123", sss: 499, ooo:3, kkk:2},
    date: "2021/6/21 16:30",
    format: "ean_13",
    key: "code",
    checked: ["name","sss"],
    inputs: [{name: "aaa", value:null}]
  },
  { code: "123",
    data: {"code": 123, name: "123", sss: 499, ooo:3, kkk:2},
    date: "2021/6/21 16:30",
    format: "ean_13",
    key: "code",
    checked: ["name","sss"],
    inputs: [{name: "aaa", value:null}]
  },
  { code: "123",
    data: {"code": 123, name: "123", sss: 499, ooo:3, kkk:2},
    date: "2021/6/22 16:30",
    format: "ean_13",
    key: "code",
    checked: ["name","sss"],
    inputs: [{name: "aaa", value:null}]
  },
]


function  dateString (d) {
  let date = new Date(Date.parse(d));
  return date.getFullYear()+"/" + ("00"+(date.getMonth()+1)).slice(-2) + "/" +
		    ("00"+(date.getDate())).slice(-2)
}


export const ResultList = {
  mixins: [MixinDebug],
  components: {
    dateitems: DateItems,
    pageloader: PageLoader,
    scanner: ScannerControl,
  },
  props: {
    debug: MixinDebug.types,
    quaggaConfig: Object,
    excelConfig: Object,
    showScanner: Boolean,
  },
  data: function () {
    this.setDebug(this.debug,"results");
    this.defaults= dateResults
    return {
      resultCodes: load('resultCodes') || this.defaults,
      datelist:null,
    }
  },
  beforeMount: function () {
    this.logger("datelist", this.datelist)
    this.datelist=this.datesOfResults(this.resultCodes);
  },

  computed: {
    date_list () {
      let list = Object.keys(this.datelist).sort((prev,next)=>next<prev ? -1:1)
      this.logger("date_list", list)
      return list
    },
  },
  methods: {
    datesOfResults (results){
      this.logger("resultCodes",results)
      let data={}, d=0, i=0
      for ( ; d<10 && i<results.length ; d++) {
	let date = dateString(results[i].date)
	data[date]= []
	while(i<results.length ? dateString(results[i].date) == date : false) {
	  data[date].unshift(results[i]);
	  i++;
	}
      }
      if( Object.keys(data).length == 0 ) data[dateString(new Date())] = []
      console.log("list result:")
      console.log(data)
      return data;
    },
    deleteItem: function(date) {
      let list = this.datelist;
      delete list[date]
      this.datelist=list
      this.resultCodes = this.resultCodes.filter(r=>dateString(r.date)!=date);
      persist('resultCodes',this.resultCodes)
      this.logger("results",this.datelist);
    },
    addItem: function(item) {
      this.logger("add DateItem",item);
      this.resultCodes.unshift(item)
      persist('resultCodes',this.resultCodes)
      let date = dateString(item.date)
      let list = this.datelist
      if( !list[date] ) {
	this.logger("new DateItem to", list);
	list[date]= []
      }
      list[date].unshift(item)
      this.datelist = list;
      this.logger("update DateItem to", this.datelist);
    }
  },
  template: `
    <div><p>履歴</p>
        <pageloader :class="{'is-active': showScanner}" style="background:#aaccdd">
  	  <slot></slot>
	  <scanner :quagga="quaggaConfig" :excel="excelConfig" :activate="showScanner" @add="addItem"></scanner>
	</pageloader>
 	<p><section class="accordions">
	  <dateitems v-for="date in date_list" :dateStr="date"
	    :results="datelist" @delete="deleteItem(date)"></dateitems>
	</section></p>
    </div>
  `,
}
