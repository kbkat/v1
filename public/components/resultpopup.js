import { MixinDebug } from "./mixin.js"

/*
export const result_data = {
   code: "123",
   format: "ean",
   data: {aaa:1,bbb:2,ccc:3},
   checked: {["aaa", "bbb"],
   inputs: [{name: "aaa", value:null}],
}
*/


export const ResultPopup = {
  mixins: [MixinDebug],
  props: {
    debug: MixinDebug.types,
    result: Object,
    popup: Boolean,
  },
  emits: ['close'],
  data () {
    this.setDebug(this.debug,"results");
    return {
      buttons: [
	{ caption: 'OK',
	  callback: this.accept
	},
	{ caption: 'X',
	  callback: this.refuse
	},
      ],
    }
  },
  methods: {
    accept() {this.logger("accept",this); this.$emit('close',this.result) },
    refuse() { this.$emit('close',null) },
  },
  
  updated() {
    this.logger("resultpopup update",this.result);
  },

  template: `
    <div class="modal" :class="{'is-active': popup}">
    <div class="modal-background" style="backgrond-color:#eeffff"></div>
    <div class="modal-card" v-if="popup">
	<header class="modal-card-head">
	  <p class="modal-card-title">{{ result.code }} ({{ result.format }})</p>
	</header>
	<section  class="modal-card-body">
	  <table v-if="!!result.data" class="table">
	    <tbody>
	      <tr v-for="col in result.checked"><td>{{ col }}</td>
		<td>{{ result.data[col] }}</td></tr>
	      <tr v-for="inp in result.inputs"><td>{{ inp.name }}</td>
		<td><input v-model="inp.value"></td>
	      </tr>
	    </tbody>
	  </table>
	  <p v-else>見つかりませんでした。</p>
	</section>
	<footer class="modal-card-foot" style="justify-content: flex-end;">
	  <button v-for="btn in buttons" type="button" class="button" @click="btn.callback">{{btn.caption}}</button>
	</footer>
    </div>
    </div>
  `,
}
