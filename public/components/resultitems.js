// -*- coding: utf-8 -*- 
import { MixinDebug } from "./mixin.js"
import{ load, persist } from "../js/Storage.js"
import{ ZMDIcon } from "./icon.js"

export const ResultItem ={
  props: {
    result:Object,
  },
  data() {
    return {
      active: false,
    }
  },
  template: `
    <div class="card   lebel is-mobile">
    <header class="card-header " @click="active=!active">
    <p class="card-header-title">
      {{ result.code }}
      (<div v-if="result.data" style="display:flex">
<span style="padding:0.5em" v-for="col in result.checked">{{ col }} : {{ result.data[col] }}, </span></div>
	<span v-else>不明</span>
      )
    </p>
  <span v-if="result.inputs" style="padding-left:1em">{{ result.inputs[0].name}}
        <input type="text" v-model="result.inputs[0].value"></span>
          <span style="padding-right: 2em">{{ TimeStr }}</span>
    </header>
    <div class="card-content" :class="{'is-active':active}">
  <span> format : {{result.format}} </span>
    <div v-if="result.data">
  <span v-for="key in Object.keys(result.data)"
	v-text="', '+key+' : '+result.data[key]"></span>
  <span v-text="result.date"></span>
    </div>
    </div>
    </div>
  `,
  computed: {
    TimeStr: function () {
      let d = new Date(this.result.date);
      return ("00"+d.getHours()).slice(-2) + ":"
	   + ("00"+d.getMinutes()).slice(-2)
    },
  }
}


export const DateItems = {
  components: {
    resultitem: ResultItem,
    zmicon: ZMDIcon
  },
  emits: ['delete'],
  props: { dateStr: String, results: Object },
  data: function() {
    return {
      active: false,
    }
  },
  computed: {
    resultItems () { return this.results[this.dateStr] }
  },
  template: `
    <article class="accordion"  :class="{'is-active':active}">
      <div class="accordion-header toggle">
    <a href="#"  @click="active=!active">{{ dateStr.slice(-5) }} ( {{ resultItems.length }} 件)</a>
<zmicon i="delete" :x="2" c="#00ffff" @click="$emit('delete',dateStr)"></zmicon>
      </div>
      <div class="accordion-body">
      <div class="accordion-content">
	<section class="accordion">
            <resultitem v-for="d in resultItems" :result="d"></resultitem>
	</section>
      </div>
      </div>
    </article>
  `,

}
