/* -*- coding: utf-8 -*- */
import { Scanner } from '../components/scanner.js'
//import { ExcelPage } from '../components/excel.js'
//import { defaultConfig } from '../components/config.js'
import { ResultPopup } from '../components/resultpopup.js'
import { MixinDebug } from '../components/mixin.js'

/* スキャナーコントローラ */
/*
   quagga のON/OFF
   読み取りコード=>エクセルデータのlookup
   結果のポップアップコントロール
   表示データの追加指示
*/
export const ScannerControl = {
  mixins: [MixinDebug],
  components: {
    result: Scanner,
    popup: ResultPopup,
  },
  props: {
    debug: MixinDebug.types,
    quagga: Object, // quaggaConfig
    excel: Object, // excelConfig
    activate: Boolean,
  },
  emits: ['add'],
  template: `
    <div style="width:80%">
    <popup :debug="debug" :popup="popup" :result="result" @close="closePopup"></popup>
     <result debug="all" :active="active" :config="quagga" @result="getResult" @detect="detect"></result>
    </div>
  `,
  data() {
    this.setDebug("all","all");
    return {
      result: null,
      code:null,
      popup: false,
      active: false,
    }
  },
  computed: {
    exConfig: function(){ return this.excelConfig(this.excel); },
  },
  beforeUpdate() {
    this.active=this.activate
  },
  methods: {
    closePopup(save) {
      if( save ) {
	this.logger("saving result", this.result)
	
	this.$emit('add', this.result);
      }
      this.popup=false
      this.active=true
    },
    getResult(val) {
      this.logger("get result", val)
      this.logger("confg", this.exConfig)
      let code = val.code
      let conf = this.exConfig
      if( conf.debug ) code = code.slice(-3)
      let data = conf.sheetData.filter(v=>code==(conf.debug?(""+v[conf.key]).slice(-3):""+v[conf.key]))
      this.logger("get data", data)
      this.result = {
	code: val.code,
	format: val.format,
	date: val.date,
	data : data.length>0 ? data[0]: null,
	checked: conf.checked,
	inputs: [{name: "aaa", value:null}]
      }
      this.logger(this.result)
      this.popup=true
      this.active=false
    },
    detect(val){
      this.logger("detect",val);
    },
    excelConfig(config) {
      if( !config ) return null;
      this.logger("update excelConfig", config)
      let file=config.file
      let target = config.target
      return {
	filename: file.filename,
	date: file.lastModifiedDate,
	sheetName: target.sheet,
	sheetData: file.sheets[target.sheet].data,
	sheetColumns: file.sheets[target.sheet].columns,
	key: target.key,
	checked: target.checked,
	debug: target.debug,
      }
    },
  },
}
