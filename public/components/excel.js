import { persist, load } from "../js/Storage.js"
import { ExcelReader } from "./excelreader.js"
import { MixinDebug } from "./mixin.js"

const excelConfig = load('excelConfig') || {}
//const excelConfig = {}
/*
const excelConfig = {
  file: {
    filename: "barcode.xlsx",
    lastModified: "2021/6/11 23:17",
    sheets: {
      "Sheet1": { data: [{ aaa: 1, bbb: 2, ccc: 3}], columns:["aaa","bbb","ccc"]},
      "Sheet2": { data: [], columns:[] },
    },
  },
  target: {
    sheet: "Sheet1",
    key: "aaa",
    checked: ["bbb"],
    debug:false,
  }
}
*/       
    
export const ExcelPage = {
  mixins: [MixinDebug],
  components: {
    excelreader: ExcelReader
  },
  props: { debug: MixinDebug.types },
  emits: [ 'update' ],
  data() {
    this.setDebug(this.debug,'excel');
    return {
      target: excelConfig.target || { sheet: "noselect", key: null, checked: [] },
      file: excelConfig.file,
    }
  },
  
  template: `
    <div>
    <excelreader :debug="debug" @load="loadExcel" :saved="file"></excelreader>
    <section>
    <select v-if="!!file" v-model="target.sheet" style="min-width:50%">
      <option v-if="target.sheet=='noselect'" value="noselect">未選択</option>
      <option v-for="name in Object.keys(file.sheets)" :value="name">{{name}}:({{file.sheets[name].data? file.sheets[name].data.length:0}}件,{{file.sheets[name].columns}})</option>
    </select>
    </section>
    <section>
    <p v-if="target.sheet!='noselect'" v-text="target.sheet"></p>
    <table v-if="!!file&&target.sheet!='noselect'" class="table">
      <thead><tr>
      <th>列名</th>
      <th>キー</th>
      <th>選択</th>
      </tr></thead>
      <tbody>
	<tr v-for="col in file.sheets[target.sheet].columns">
	  <td>{{ col }}</td>
          <td><input :value="col" type="checkbox" :checked="col==target.key" @click="checkKey"></td>
	  <td><input :value="col" type="checkbox" :checked="target.checked.indexOf(col)>=0" @click="checkColumn"></td>
	</tr>
      </tbody>
    </table>
     <div style="padding-bottom:1em">   <span v-if="target.sheet!='noselect'">デバッグ <input type="checkbox" v-model="target.debug"></span>
       </div>
    </section>
    <section class="columns is-mobile" style="width:50vw;background:#eeeeee">
      <a class="column is-link" @click="update(true)">保存</a>
      <a class="column is-link" @click="cancel">キャンセル</a>
      <a class="column is-link" @click="reset">リセット</a>
    </section>
    </div>
  `,
  
  mounted() {
    this.update(false)
  },
  
  beforeUpdate() {
    console.log("excel updating");
  },

  
  methods: {
    cancel() {
      this.$emit('update', { file: this.file, target: this.target });
    },
    reset() {
      this.target = { sheet: "noselect", key:null, checked:[] }
      this.file = null
    },
    
    update(save) {
      this.logger("save",this.target);
      this.logger("save",this.file);
      this.target.checked = this.target.checked.filter(
	c=>this.file.sheets[this.target.sheet].columns.indexOf(c)>=0)
      let config = { file: this.file, target: this.target };
      if( save ) persist('excelConfig',config)
      this.$emit('update',config)
    },
    
    loadExcel(file) {
      this.file=file
      if( !file.sheets[this.target.sheet] ) {
	this.target={ sheet: "noselect", key:null, checked:[] }
      }
    },
    
    checkKey(ev) {
      let col = ev.target.value
      if( ev.target.checked ) {
	this.logger("checkKey",ev);
	this.target.key = col
	let i = this.target.checked.indexOf(col)
	if( i >= 0 )  this.target.checked.splice(i,1)
      } else {
	this.target.key = null
      }
    },
    checkColumn(ev) {
      let col=ev.target.value
      this.logger("checkColumn",ev)
      if( this.target.key == col ) {
	let i = this.target.checked.indexOf(col)
	if( i>= 0) this.target.checked.splice(i,-1)
	return
      }
      
      if( ev.target.checked ) {
	this.target.checked.push(ev.target.value)
      } else {
	let i = this.target.checked.indexOf(ev.target.value)
	this.target.checked.splice(i,1)
      }
    }
  },
}
  
