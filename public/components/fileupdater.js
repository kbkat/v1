import { MixinDebug } from "./mixin.js"

export const FileUpdater = {
  mixins: [MixinDebug],
  props: { debug: MixinDebug.types, filedata: Object },
  emits: ['selected'],
  
  data() {
    this.setDebug(this.debug,"fileupdater");
    return {
      update: null,
      file:  !!this.filedata ? { name: this.filedata.filename, lastModifiedDate: this.filedata.lastModified }: { name: "未設定", lastModifiedDate: "---" }
    }
  },

  computed: {
    input: function() {
      return  '<input style="display:none" type="file" date="' + this.update + '">'
    }
  },
      
  template: `
    <div style="display:flex"><input type="button" value="更新" @click="fileupdate"><span v-if="!!file">{{ file.name }} : [{{ file.lastModifiedDate }} ]</span></div>
     <div ref="fileinput" :update="update" style="display:none" v-html="input" @change="fileSelect">
     </div>
  `,
  
  beforeUpdate() {
    if( this.debug ) console.log("updating")
  },
  
  methods: {
    debug_log(msg) {
      if( this.debug ){
	console.log(this);
	console.log(msg);
      }
    },
      
    fileupdate() {
      this.$refs['fileinput'].childNodes[0].click();
    },

    fileSelect(ev) {
      this.file = ev.target.files[0]
      this.$emit('selected', this.file)
      this.update = this.file.lastModifiedDate
      this.debug_log(this.file)
    },
  }
}
