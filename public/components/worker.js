export const UpdateWorker= {
  props: {
    doupdate: String
  },
  data() {
    return {
      updated: null
    }
  },
  emits : ['update', 'version'],
  template: `
    <div id="worker">
      worker {{ updated }}
    <input type="button" value="reload" @click="reload(false)">
    <input type="button" value="allreload" @click="reload(true)">
    </div>
  `,

  mounted() {
    if ('serviceWorker' in navigator) {
      let self_=this;
      /* message event の受付 */
      navigator.serviceWorker.addEventListener('message',this.sw_update);
      /* 現在のworkerでのバージョンを取得 */
      navigator.serviceWorker.ready.then(registration=>{
	registration.active.postMessage({'command': 'getVersion'})
      });
      /* workerの更新確認 */
      navigator.serviceWorker.register('./sw.js', { scope: './' })
	       .then(function(reg) {
		 console.log('登録に成功しました。 Scope は ' + reg.scope);
		 reg.onupdatefound = self_.updateFound
	       })
	       .catch(function(error) {
		 console.log('登録に失敗しました。' + error);
	       })
    }
  },

  beforeUpdate() {
    console.log("ws doupdate");
    switch ( this.doupdate ){
      case 'update':
	this.reload(true);
	break;
      case 'check':
	this.check()
	break;
    }
  },
  methods: {
    check() {
      navigator.serviceWorker.getRegistration()
	       .then(reg=>reg.update());
    },
    
    updateFound(e) {
      console.log("update found");
      console.log(e.target.installing)
      if(e.target.installing) {
	e.target.installing.onstatechange = this.stateChange
      }
    },
    stateChange(e) {
      console.log("state Change");
      console.log(e.target.state)
      if( e.target.state == 'installed' ) {
	e.target.postMessage({command:'skipWaiting'})
      }
    },
    
    reload(force) {
      if( force ) {
	console.log("clearCache and reload")
	this.skipWaiting(()=>location.reload(force));
      } else {
	window.location.reload()
      }
    },
    sw_update(msg) {
      console.log("cl get message");
      console.log(msg);
      switch (msg.data.msg) {
	case 'update':
	  this.$emit('update',msg.data.app);
	  break;
	case 'version':
	  this.$emit('version',msg.data.app)
	  break;
	case 'new':
	  this.$emit('update', msg.data.app);
	  break;
      }
    },
    skipWaiting(callback) {
      if ('serviceWorker' in navigator) {
	navigator.serviceWorker
		 .ready.then(reg=>{
		   reg.active.postMessage({command: 'clearCache'})
		   return reg
		 })
		 .then(reg=>{
		   console.log(reg);
		   if(!!reg.waiting && reg.waiting.state=='installed') {
		     reg.waiting.postMessage({'command': 'skipWaiting'})
		   }
		   return reg
		 })
		 .then(callback);
      }
    },
    
    clearCache(callback) {
      if ('serviceWorker' in navigator) {
	navigator.serviceWorker.ready.then(registration=>{
	  registration.active.postMessage({'command': 'clearCache'})
	})
		 .then(callback);
      }
    }
  },
}
