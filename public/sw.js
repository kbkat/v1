var CACHE_NAME  = "barcode-v1";
var SW_VERSION = "2"
var APP_VERSION = "0.2.3"
//キャッシュしないドメイン名のリスト
var noCacheList = [
  //  "localhost"
];

self.addEventListener('activate', function(e) {
  console.log('sw activate')
  var currentCache = [CACHE_NAME];

  e.waitUntil(
    caches.has(CACHE_NAME)
	  .then(hasCache=>{
	    self.clients.matchAll().then(clients=>{
	      clients.forEach(client=>{
	    	client.postMessage({msg: 'update', app: APP_VERSION, updated: new Date()})})});
	    caches.keys()
		  .then(keyList=>{
		    return Promise.all(keyList.map(key=>{
		      if( currentCache.indexOf(key) === -1 ){
			return caches.delete(key)
		      }
		    }))
		  })
	  })
  )
})


//インストール時の処理
self.addEventListener('install', function(e) {
  console.log("sw installing");
  e.waitUntil(
    caches.has(CACHE_NAME)
	  .then(hasCache=>{
	    if( !hasCache ) {  /* キャッシュ名が変わったら,強制activate */
	      self.skipWaiting()
	    }
	  })
  )
})


//フェッチ時（何かしらの通信リクエスト）があったときの処理
self.addEventListener('fetch', function (e) {
  e.respondWith(
    caches.open(CACHE_NAME).then(function (cache) {
      return cache.match(e.request).then(function (response) {
        if (response) {
          // e.requestに対するキャッシュが見つかったのでそれを返却
          return response;
        } else {
          // キャッシュが見つからなかったので取得
          return fetch(e.request.clone()).then(function (response) {
            //キャッシュしないドメイン名のリストに含まれていないかを確認する
            var url = e.request.url;
            var flg = true;
            for(var cnt=0;cnt<noCacheList.length;cnt++){
              if(url.indexOf(noCacheList[cnt])!=-1){
                flg = false;
              }
            }
            if(flg){
              //キャッシュに追加する
//	      console.log("put cache")
//	      console.log(e.request)
              cache.put(e.request, response.clone());
            }else{
              //リストにあったのでキャッシュ追加はしない。
            }
            // 取得したリソースを返却
            return response;
          });
        }
      });
    })
  );
});

//メッセージ受信時の処理
self.addEventListener('message', function(e) {
  console.log("sw get Message");
  console.log(e.data);
  switch (e.data['command']) {
    case 'getVersion':
      e.waitUntil(
	self.clients.matchAll().then(clients=>{
	  clients.forEach(client=>{
	    client.postMessage({msg: 'version', sw: SW_VERSION, app: APP_VERSION})
	  })}));
      break;
    case 'getNew':
      e.waitUntil(
	self.clients.matchAll().then(clients=>{
	  clients.forEach(client=>{
	    client.postMessage({msg: 'new', sw: SW_VERSION, app: APP_VERSION})
	  })}))
      break;
    case 'skipWaiting':
      e.waitUntil(self.skipWaiting());
      break;
    case 'clearCache':
      //キャッシュをクリアする
      e.waitUntil(caches.delete(CACHE_NAME));
      break;
    case 'getCache':
      //URLリストのキャッシュを取得する
      e.waitUntil((function() {
        caches.open(CACHE_NAME).then((cache) => {
          this.console.log(e.data['files']);
          // 指定されたリソースをキャッシュに追加する
          return cache.addAll(e.data['files']);
        })})()
      );
      break;
  }
});
